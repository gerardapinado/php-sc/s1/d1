 <?php require_once "./code.php"; ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>S1: PHP Basics and Selections Control Structures</title>
 </head>
 <body>
     <h1>Echoing Values</h1>
     <!-- Concatination -->
     <?php echo "Good day ".$name."! your given email is ".$email."." ; ?>
     <!-- Double quotes -->
     <p><?php echo"Good day $name! your given email is $email"?></p>
     <!-- Constant variables -->
     <p><?php echo PI; ?></p>
     <p><?php echo "Your address is $address"; ?></p>
    <!-- Data Types -->
     <p><?php echo $hasTraveled; ?></p>
     <p><?php echo $spouse; ?></p>
     <!-- echo bool & null -->
     <p><?php echo var_dump($spouse); ?></p>
     <p><?php echo gettype($hasTraveled); ?></p>
    <!-- output object -->
    <p><?php echo $person->address->state; ?></p>
    <p><?php echo $grades[2]; ?></p>
    <!-- Operators -->
    <p>SUM: <?php echo $x+$y; ?></p>
    <p>DIFF: <?php echo $x-$y; ?></p>
    <p>QUO: <?php echo $x/$y; ?></p>
    <p>PROD: <?php echo $x*$y; ?></p>

    <p>Lose Equality: <?php echo var_dump($x == '1234'); ?></p>
    <p>Strict Equality: <?php echo var_dump($x === '1234'); ?></p>
    <!-- Functions -->
    <p><?php echo getFullName("gerard", "nunez", "apinado"); ?></p>

    <h1>Selection Control Structures</h1>
    <h2>If-ElseIf-Else</h2>
    <p><?php echo getTyphoonIntensity(55); ?></p>

    <p><?php echo var_dump(isUnderAge(18)); ?></p>
    <p><?php echo var_dump(isUnderAge(17)); ?></p>


 </body>
 </html>

