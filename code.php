<?php

/*
    [SECTION] Comments
    Single Line: //
    Multi Line: enclosed w/ /*

    [SECTION] Variables
    Variables are used to contain data
    Declaration: $ notation
    eg: $name 
*/
$name = "John Smith";
$email = "johnsmith@gmail.com";
// variable reassignment
$name = "John Johnson";

/*
    [SECTION] Constants
    Constants used to hold immutable data
    define("var_name", value)
    Naming Convention: CAPITALIZED
*/
define("PI", 3.1416);

/*
    [SECTION] Data Types
*/
// String
$state = "New York";
$country = "USA";
$address = $state . ", " . $country;
$address = "$state, $country";
// Integer
$age = 31;
$headcount = 26;
// Float
$grade = 98.2;
$distaceKm = 1342.12;
// Boolean
$hasTraveled = false;
$hasSymptoms = true;
// Null
$spouse = null;
// Objects 
$person = (object)[
    'fullname' => 'Jhon Smith',
    'isMarried' => false,
    'age' => 35,
    'address' => (object)[
        'state' => 'New York',
        'country' => 'United States of America'
    ]
];

// Arrays
$grades = array(98.6, 92.1, 90.2, 94.6);

/*
    [SECTION] Operators
*/
// Assignment
$x = 1234;
$y = 1245;

$isLegalAge = true;
$isREgistered = false;

/* 
    [SECTION] Functions
*/

function getFullName ($fName, $mName, $lName){
    return("$fName $mName $lName");
}

function getTyphoonIntensity($windspeed){
    if($windspeed < 30){
        return 'not a typhoon';
    }else if($windspeed <= 61){
        return 'tropical depression';
    }else if($windspeed >= 62 && $windspeed <= 88){
        return 'tropical storm';
    }else if($windspeed >= 89 && $windspeed <= 117){
        return 'severe storm';
    }else {
        return 'typhoon';
    }
}
// Ternary Operators
function isUnderAge($age){
    return ($age <18) ? true : false;
}